package ru.tsc.chertkova.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showVersion();

    void showAbout();

    void showError(String param);

    void showInfo();

    void showCommands();

    void showArguments();

}
