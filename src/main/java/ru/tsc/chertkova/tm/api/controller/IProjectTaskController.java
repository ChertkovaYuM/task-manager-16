package ru.tsc.chertkova.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

    void removeProjectById();

}
